
PATH=$HOME/.rvm/bin:$HOME/.rbenv/bin:$HOME/.go:/usr/sbin:$HOME/.cask/bin:$PATH

export EDITOR=emacsclient
export VISUAL=emacsclient
export GOPATH=$HOME/.go

if [ -d "$HOME/.go" ]; then
    PATH=$HOME/.go/bin:$PATH
fi    

#for rvm
if [ -f "$HOME/.rvm/scripts/rvm" ]; then
    source "$HOME/.rvm/scripts/rvm"
fi


if [ -d "$HOME/.rbenv" ]; then
  eval "$(rbenv init -)"
fi

HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000
setopt hist_ignore_dups     # ignore duplication command history list
setopt share_history        # share command history data


# use emacs style key-bindings
bindkey -e

#prompt settings
#0: root
#*: other

#%m The hostname up to the first dot(.)
#%~ The current directory($PWD)
#%1~ The last part of the current directory

#reference
#http://zsh.sourceforge.net/Guide/zshguide02.html#l19
#http://www.acm.uiuc.edu/workshops/zsh/prompt/escapes.html
#http://blog.blueblack.net/item_207

case ${UID} in
0)
    PROMPT="%B%{[31m%}%/#%{[m%}%b "
    PROMPT2="%B%{[31m%}%_#%{[m%}%b "
    SPROMPT="%B%{[31m%}%r is correct? [n,y,a,e]:%{[m%}%b "
    [ -n "${REMOTEHOST}${SSH_CONNECTION}" ] && 
        PROMPT="%{[47m%}${HOST%%.*} ${PROMPT}"
    ;;
*)
    #PROMPT="%B%{[32m%}%m%%%{%1~[m%}%b "
    local lf=$'\n' 
    PROMPT="%B%{[32m%}%6(~|[%~]$lf|)%m@%n%%%b "
    RPROMPT="%6(~||[%~]) "
    PROMPT2="%{[32m%}%_%%%{^[[m%} "
    SPROMPT="%{32m%}%r is correct? [n,y,a,e]:%{[m%} "
    [ -n "${REMOTEHOST}${SSH_CONNECTION}" ] && 
        PROMPT="%{[44m%}${HOST%%.*} ${PROMPT}"
    ;;
esac

preexec () {
    if [ $TERM = "screen" ]; then
    command="${(z)2}"
    array=(`echo $command`) # String to Array
    title=(`echo "$array[1] $array[2]" | cut -c1-15`)
    echo -ne "\ek[$title]\e\\"
  fi
}

if ! which pbcopy >/dev/null 2>&1 ; then
    alias pbcopy='xsel --clipboard --input'
    alias pbpaste='xsel --clipboard --output'   
fi


function peco-snippets() {
    line=$(grep -v '^#' ~/.sheets/* | cut -d':' -f2- | peco --query "$LBUFFER")
    echo -n $line | pbcopy
    BUFFER=$line
    zle clear-screen
}

zle -N peco-snippets
bindkey '^j' peco-snippets

# command alias
alias ll="ls -al"
alias cp="cp -ipPR"

