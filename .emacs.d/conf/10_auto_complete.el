;; auto-complete有効
;;(require 'auto-complete)
;;(global-auto-complete-mode t)
(when (require 'auto-complete-config nil t)
  (add-to-list 'ac-dictionary-directories
                "~/.emacs.d/elisp/ac-dict")
               (define-key ac-mode-map (kbd "M-TAB") 'auto-complete)
               (ac-config-default))
