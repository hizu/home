;;; 10_key_chord.el ---

(require 'key-chord)
(setq key-chord-two-keys-delay 0.01)
(key-chord-mode 1)

(key-chord-define-global "jj" 'forward-word)
(key-chord-define-global "bb" 'backward-word)