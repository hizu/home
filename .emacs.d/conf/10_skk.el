;; Use SKK
(let (
      (default-directory (expand-file-name "~/.emacs.d/elisp/skk"))
      )
  (add-to-list 'load-path default-directory)
  (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
      (normal-top-level-add-subdirs-to-load-path)))
(require 'skk-autoloads)

(setq default-input-method "japanese-skk")
(setq skk-henkan-strict-okuri-precedence t)
(setq skk-check-okurigana-on-touroku t)
(setq skk-large-jisyo "~/.emacs.d/share/skk/SKK-JISYO.L")
(setq skk-tut-file    "~/.emacs.d/share/skk/SKK.tut")
(setq skk-kutouten-type 'en)
(setq skk-sticky-key ";")
(global-set-key (kbd "\C-m") 'newline-and-indent)
(global-set-key "\C-x\C-j" 'skk-mode)
(global-set-key "\C-xj" 'skk-auto-fill-mode)
(global-set-key "\C-xt" 'skk-tutorial)
