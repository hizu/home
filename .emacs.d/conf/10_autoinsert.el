(auto-insert-mode)

(setq auto-insert-directory "~/.emacs.d/insert/")
(define-auto-insert "\\.rb$" "ruby-template.rb")
(define-auto-insert "\Gemfile$" "Gemfile")