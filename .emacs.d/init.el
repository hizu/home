;;; P30 デバッグモードでの起動
;; おまじない
(require 'cl)
;; Emacsからの質問をy/nで回答する
;; (fset 'yes-or-no-p 'y-or-n-p)
;; スタートアップメッセージを非表示
(setq inhibit-startup-screen t)

;; load-path を追加する関数を定義
(defun add-to-load-path (&rest paths)
  (let (path)
    (dolist (path paths paths)
      (let ((default-directory
              (expand-file-name (concat user-emacs-directory path))))
        (add-to-list 'load-path default-directory)
        (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
            (normal-top-level-add-subdirs-to-load-path))))))

;; 引数のディレクトリとそのサブディレクトリをload-pathに追加
(add-to-load-path "elisp" "public_repos")

;; init-loader
(require 'init-loader)
(init-loader-load "~/.emacs.d/conf")

;;key bind
(global-set-key "\C-h" 'delete-backward-char)
(global-set-key (kbd "C-m") 'newline-and-indent)
(global-set-key "\C-w" 'nil)
(global-set-key "\C-w\C-k" 'kill-region)
(global-set-key "\C-@" 'anything-execute-extended-command)


;;タブ幅を 4 に設定
(setq-default tab-width 4)
;;タブ幅の倍数を設定
(setq tab-stop-list
  '(4 8 12 16 20 28 24 32 36 40 44 48 52 56 60))
;;タブではなくスペースを使う
(setq-default indent-tabs-mode nil)
(setq indent-line-function 'indent-relative-maybe)


;; auto-install.el
(add-to-load-path "auto-install")
(require 'auto-install)
(setq auto-install-directory "~/.emacs.d/elisp/")
;;起動時にEmacs wikiのページ名を補完候補に加える
;(auto-install-update-emacswiki-package-name t)
;;install-elisp.el互換モードにする
(auto-install-compatibility-setup)
;; ediff関連のバッファを1つにまとめる
(setq ediff-window-setup-function 'ediff-setup-windows-plain)

(require 'anything)
(defvar org-directory "")
(require 'anything-startup)

;; より下に記述した物が PATH の先頭に追加されます
(dolist (dir (list
              "/sbin"
              "/usr/sbin"
              "/bin"
              "/usr/bin"
              "/opt/local/bin"
              "/usr/local/bin"
              ))

  ;; PATH と exec-path に同じ物を追加します
 (when (and (file-exists-p dir) (not (member dir exec-path)))
   (setenv "PATH" (concat dir ":" (getenv "PATH")))
   (setq exec-path (append (list dir) exec-path))))

;;backup fileを作らない
(setq make-backup-files nil)

(server-start)
(defun iconify-emacs-when-server-is-done ()
  (unless server-clients (iconify-frame)))

(add-hook 'server-done-hook 'iconify-emacs-when-server-is-done)

(global-set-key (kbd "C-x C-c") 'server-edit)

;; uniquify
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(setq uniquify-ignore-buffers-re "*[^*]+*")

;; cask
(require 'cask "~/.cask/cask.el")
(cask-initialize)
